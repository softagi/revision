package softagi.mansour.revision.network.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import softagi.mansour.revision.models.userModel;

@Database(entities = {userModel.class}, version = 1)
public abstract class usersDatabase extends RoomDatabase
{
    public abstract usersDao usersDao();
}