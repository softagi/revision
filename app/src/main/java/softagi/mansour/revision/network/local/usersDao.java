package softagi.mansour.revision.network.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import softagi.mansour.revision.models.userModel;

@Dao
public interface usersDao
{
    @Insert
    void insertUser(userModel userModel);

    @Query("SELECT * FROM usersTable")
    List<userModel> getAllUsers();

    @Update
    void updateUser(userModel userModel);

    @Delete
    void deleteUser(userModel userModel);

    @Query("SELECT * FROM usersTable WHERE userName = (:user)")
    List<userModel> getUserByName(String user);
}