package softagi.mansour.revision.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "usersTable")
public class userModel
{
    @ColumnInfo(name = "userId")
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "userName")
    private String name;

    @ColumnInfo(name = "userAddress")
    private String address;

    @ColumnInfo(name = "userMobile")
    private String mobile;

    @ColumnInfo(name = "userDescription")
    private String description;

    public userModel(String name, String address, String mobile, String description) {
        this.name = name;
        this.address = address;
        this.mobile = mobile;
        this.description = description;
    }

    @Ignore
    public userModel(int id, String name, String address, String mobile, String description) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.mobile = mobile;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}