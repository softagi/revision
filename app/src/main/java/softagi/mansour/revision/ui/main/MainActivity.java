package softagi.mansour.revision.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import softagi.mansour.revision.R;
import softagi.mansour.revision.ui.home.homeFragment;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startFragment();
    }

    private void startFragment()
    {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.main_container, new homeFragment())
                .commit();
    }
}