package softagi.mansour.revision.ui.users;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import java.util.List;

import softagi.mansour.revision.R;
import softagi.mansour.revision.models.userModel;
import softagi.mansour.revision.network.local.usersDatabase;
import softagi.mansour.revision.ui.addUser.addUserFragment;

public class usersFagment extends Fragment
{
    private View mainView;
    private RelativeLayout searchRelative;
    private RecyclerView recyclerView;
    private LinearLayout emptyLinear;
    private ImageView searchBtn;
    private EditText searchField;
    private usersDatabase database;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.fragment_users, null);
        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initViews();
        initDb();
        new getUsers().execute();
    }

    private void initDb()
    {
        database = Room.databaseBuilder(requireContext(), usersDatabase.class, "usersDB").build();
    }

    private void initViews()
    {
        recyclerView = mainView.findViewById(R.id.users_recycler);

        emptyLinear = mainView.findViewById(R.id.empty_linear);

        searchRelative = mainView.findViewById(R.id.search_relative);
        searchField = mainView.findViewById(R.id.search_field);
        searchBtn = mainView.findViewById(R.id.search_btn);

        searchField.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                searchRelative.setVisibility(View.VISIBLE);
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String name = searchField.getText().toString();

                if (name.isEmpty())
                {
                    Toast.makeText(requireContext(), "invalid data", Toast.LENGTH_SHORT).show();
                    return;
                }

                new searchUser().execute(name);
            }
        });
    }

    class usersAdapter extends RecyclerView.Adapter<usersAdapter.VH>
    {
        List<userModel> models;

        usersAdapter(List<userModel> models)
        {
            this.models = models;
        }

        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(requireContext()).inflate(R.layout.user_item, parent, false);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull VH holder, int position)
        {
            final userModel userModel = models.get(position);

            String name = userModel.getName();
            String address = userModel.getAddress();
            String mobile = userModel.getMobile();
            int id = userModel.getId();

            holder.nameText.setText(name);
            holder.addressText.setText(address);
            holder.mobileText.setText(mobile);
            holder.idText.setText(String.valueOf(id));

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick(View v)
                {
                    new deleteUser().execute(userModel);
                    return false;
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    requireActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_container, new addUserFragment(userModel))
                            .addToBackStack(null)
                            .commit();
                }
            });
        }

        @Override
        public int getItemCount()
        {
            return models.size();
        }

        class VH extends RecyclerView.ViewHolder
        {
            TextView nameText,addressText,mobileText,idText;

            VH(@NonNull View itemView)
            {
                super(itemView);

                nameText = itemView.findViewById(R.id.user_name_text);
                addressText = itemView.findViewById(R.id.user_address_text);
                mobileText = itemView.findViewById(R.id.user_mobile_text);
                idText = itemView.findViewById(R.id.user_id_text);
            }
        }
    }

    class getUsers extends AsyncTask<Void, Void, List<userModel>>
    {
        @Override
        protected List<userModel> doInBackground(Void... voids)
        {
            return database.usersDao().getAllUsers();
        }

        @Override
        protected void onPostExecute(List<userModel> userModels)
        {
            super.onPostExecute(userModels);

            if (userModels.size() != 0 && userModels != null)
            {
                emptyLinear.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.setAdapter(new usersAdapter(userModels));
            } else
                {
                    emptyLinear.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
        }
    }

    class deleteUser extends AsyncTask<userModel, Void, Void>
    {
        @Override
        protected Void doInBackground(userModel... userModels)
        {
            database.usersDao().deleteUser(userModels[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            new getUsers().execute();
        }
    }

    class searchUser extends AsyncTask<String, Void, List<userModel>>
    {
        @Override
        protected List<userModel> doInBackground(String... strings)
        {
            return database.usersDao().getUserByName(strings[0]);
        }

        @Override
        protected void onPostExecute(List<userModel> userModels)
        {
            super.onPostExecute(userModels);
            if (userModels != null && userModels.size() != 0)
            {
                recyclerView.setAdapter(new usersAdapter(userModels));
            }
        }
    }
}