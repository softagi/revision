package softagi.mansour.revision.ui.addUser;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import softagi.mansour.revision.R;
import softagi.mansour.revision.models.userModel;
import softagi.mansour.revision.network.local.usersDatabase;

public class addUserFragment extends Fragment
{
    private View mainView;
    private EditText nameField;
    private EditText addressField;
    private EditText mobileField;
    private EditText descriptionField;
    private Button addBtn;
    private usersDatabase database;
    private userModel um;

    public addUserFragment(userModel userModel)
    {
        um = userModel;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.fragment_add_user, null);
        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initViews();
        initDb();

        if (um != null)
        {
            nameField.setText(um.getName());
            addressField.setText(um.getAddress());
            mobileField.setText(um.getMobile());
            descriptionField.setText(um.getDescription());

            addBtn.setText("update");

            update(um);
        } else
            {
                addUser();
            }
    }

    private void initDb()
    {
        database = Room.databaseBuilder(requireContext(), usersDatabase.class, "usersDB").build();
    }

    private void addUser()
    {
        addBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String name = nameField.getText().toString();
                String address = addressField.getText().toString();
                String mobile = mobileField.getText().toString();
                String description = descriptionField.getText().toString();

                if (name.isEmpty() || address.isEmpty() || mobile.isEmpty() || description.isEmpty())
                {
                    Toast.makeText(requireContext(), "invalid data", Toast.LENGTH_SHORT).show();
                    return;
                }

                userModel userModel = new userModel(name, address, mobile, description);
                new insertUser().execute(userModel);
            }
        });
    }

    private void update(final userModel um)
    {
        addBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String name = nameField.getText().toString();
                String address = addressField.getText().toString();
                String mobile = mobileField.getText().toString();
                String description = descriptionField.getText().toString();

                if (name.isEmpty() || address.isEmpty() || mobile.isEmpty() || description.isEmpty())
                {
                    Toast.makeText(requireContext(), "invalid data", Toast.LENGTH_SHORT).show();
                    return;
                }

                userModel userModel = new userModel(um.getId(),name, address, mobile, description);
                new updateUser().execute(userModel);
            }
        });
    }

    private void initViews()
    {
        nameField = mainView.findViewById(R.id.name_filed);
        addressField = mainView.findViewById(R.id.address_filed);
        mobileField = mainView.findViewById(R.id.mobile_filed);
        descriptionField = mainView.findViewById(R.id.description_filed);
        addBtn = mainView.findViewById(R.id.add_new_user_btn);
    }

    class insertUser extends AsyncTask<userModel, Void, Void>
    {
        @Override
        protected Void doInBackground(userModel... userModels)
        {
            database.usersDao().insertUser(userModels[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            requireActivity().onBackPressed();
        }
    }

    class updateUser extends AsyncTask<userModel, Void, Void>
    {
        @Override
        protected Void doInBackground(userModel... userModels)
        {
            database.usersDao().updateUser(userModels[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            requireActivity().onBackPressed();
        }
    }
}