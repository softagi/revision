package softagi.mansour.revision.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import softagi.mansour.revision.R;
import softagi.mansour.revision.ui.addUser.addUserFragment;

public class homeFragment extends Fragment
{
    private View mainView;
    private FloatingActionButton addUser;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.fragment_home, null);
        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        setupBottom();
        initViews();
    }

    private void initViews()
    {
        addUser = mainView.findViewById(R.id.add_user_fab);

        addUser.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                requireActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_container, new addUserFragment(null))
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    private void setupBottom()
    {
        BottomNavigationView bottomNavigationView = mainView.findViewById(R.id.navigation);
        NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_home);

        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }
}